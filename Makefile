.SUFFIXES: .o .c

CC=gcc
BDG=gdb
RM=rm -frv
MKDIR=mkdir -p
ECHO=echo

ARCH=

CFLAGS=-c -Wall $(ARCH)
LDFLAGS=-static $(ARCH)
DBGFLAGS=-ggdb

SRCDIR=src/
INCDIR=include/
OBJDIR=obj/
BINDIR=bin/

LIBS=-lm -lrt

DIRS=$(SRCDIR) $(INCDIR) $(OBJDIR) $(BINDIR)

NAME=benchmark
BINNAME=$(BINDIR)/$(NAME)

_OBJS=$(NAME).o dft.o fft.o cli.o
OBJS=$(patsubst %,$(OBJDIR)/%,$(_OBJS))


all: dirs $(BINNAME)
	@$(ECHO) Hecho.

$(BINNAME): $(OBJS) $(LIBS)
	@$(ECHO) Enlazando código objeto del ejecutable...
	$(CC) $(LDFLAGS) $(OBJS) $(LIBS) -o $@

$(OBJDIR)%.o: $(SRCDIR)%.c $(INCDIR)%.h
	$(CC) $(CFLAGS) -I$(INCDIR) $< -o $@


.PHONY: clean dirs debug

dirs: $(DIRS)
	@$(ECHO) Generados directorios $(OBJDIR) y $(BINDIR).
	@$(ECHO) Compilando código fuente...

$(DIRS):
	@for dir in $(DIRS); do \
		$(MKDIR) $$dir; \
	done

clean:
	$(RM) core
	$(RM) $(BINDIR)
	$(RM) $(OBJDIR)
	$(RM) $(INCDIR)/*~
	$(RM) $(SRCDIR)/*~
	$(RM) ./*~

debug: $(OBJS_BM) $(LIBS)
	@$(ECHO) Enlazando ejecutables con datos de depuración...
	$(CC) $(LDFLAGS) $(DBGFLAGS) $(OBJS_BM) $(LIBS) -o $(BINNAME_BM)


