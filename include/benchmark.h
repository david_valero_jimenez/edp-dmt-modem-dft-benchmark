/*
 * END-OF-DEGREE PROJECT: DESIGN AND SIMULATION OF DMT MODEM
 * BENCHMARKING PROGRAM OF HIGH-LEVEL ALGORITHMS FOR DFT COMPUTING
 *
 * David Valero Jimenez
 * Degree in Computer Science, Intensification in Computation
 * University of Castilla - La Mancha, Ciudad Real, Spain
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "cli.h"
#include "complex.h"
#include "dft.h"
#include "fft.h"


#define NT ((unsigned int)4)

typedef enum {DFT, IDFT, FFTR2, IFFTR2} algorithm;

void randomInitialize(unsigned int N, double **data);
