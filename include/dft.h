#ifndef DFT_H
#define DFT_H

#include <stdlib.h>
#include <math.h>

#include "complex.h"


/* Direct DFT algorithm implementation */
double **_dft_idft(int N, double **input, int inv);

/* Driver functions for DFT and IDFT */
double **dft(int N, double **x);
double **idft(int N, double **X);


#endif
