#ifndef CLI_H
#define CLI_H

#include <stdio.h>
#include <stdlib.h>

#define DATA_NAME "benchmark.dat"
#define CREDITS "David Valero Jimenez\nUniversity of Castilla - La Mancha, Ciudad Real, Spain\n\n"

void help(char *bin_name);
int checkCommandLine(int argc, char **argv, unsigned int *NS, unsigned int **size);
void writeOutput(unsigned int *size, long **benchmark, unsigned int NS, unsigned int NT);

#endif
