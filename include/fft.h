#ifndef FFT_H
#define FFT_H

#include <stdlib.h>
#include <math.h>

#include "complex.h"


/* Radix-2 Fast Fourier Transform algorithm implementation */
double **_fft_ifft_r2(int N, double **input, int inv);

/* Driver function for radix-2 FFT and IFFT */
double **fft_r2(int N, double **x);
double **ifft_r2(int N, double **X);

/* In-place complex matrix bit-reverse function */
void bitReverse(int N, double **input);


#endif
