#include "benchmark.h"


int main(int argc, char **argv) {
  unsigned int NS;
  unsigned int *size;
  double **data;
  int i, j;
  long **benchmark;
  struct timespec t1, t2;
  

  /* Command line checking */
  if(checkCommandLine(argc, argv, &NS, &size)) {
    help(argv[0]);
    exit(1);
  }

  /* Memory allocation */
  benchmark = (long **)malloc(sizeof(long *)*NS);
  for(i=0;i<NS;i++) {
    benchmark[i] = (long *)malloc(sizeof(long)*NT);
  }

  data = (double**)malloc(2*sizeof(double*));
  

  /* Execution time measurement using clock_gettime to get CPU time of process */
  for(i=0;i<NS;i++) {
    printf("[ %i-POINT TESTS ]\n", size[i]);
    data[REAL] = (double*)malloc(size[i]*sizeof(double));
    data[IMAG] = (double*)malloc(size[i]*sizeof(double));

    for(j=0;j<NT;j++) {
      randomInitialize(size[i], data);

      switch(j) {
      case DFT:
	printf(" >DFT..."); fflush(stdout);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
	dft(size[i], data);
	break;
      case IDFT:
	printf(" >IDFT..."); fflush(stdout);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
	idft(size[i], data);
	break;
      case FFTR2:
 	printf(" >FFT..."); fflush(stdout);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
	fft_r2(size[i], data);
	break;
      case IFFTR2:
	printf(" >IFFT..."); fflush(stdout);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
	ifft_r2(size[i], data);
	break;
      }
      clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);

      benchmark[i][j] = (t2.tv_sec*1000000000+t2.tv_nsec) - (t1.tv_sec*1000000000+t1.tv_nsec);

      printf(" done.\n");
    }

    free(data[REAL]);
    free(data[IMAG]);
  }


  /* Results */
  writeOutput(size, benchmark, NS, NT);


  free(size);

  exit(0);
  return 0;
}


/* Function to initialize complex matrix with random floats */
void randomInitialize(unsigned int N, double **data) {
  int i;

  srand(time(NULL));

  for(i=0;i<N;i++) {
    data[REAL][i] = (double)random()/random();
    data[IMAG][i] = (double)random()/random();
  }
}
