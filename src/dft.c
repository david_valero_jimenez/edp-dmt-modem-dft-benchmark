#include "dft.h"


double **dft(int N, double **x) {
  return _dft_idft(N, x, 0);
}


double **idft(int N, double **X) {
  return _dft_idft(N, X, 1);
}


double **_dft_idft(int N, double **input, int inv) {
  double  WN__, WN_, WN;
  double **output, *oR, *oI, *iR, *iI;
  double c, s;
  int k, n;

  /* Allocate output matrix memory */
  output = (double **)malloc(sizeof(double *)*2);
  oR = output[REAL] = (double *)malloc(sizeof(double)*N);
  oI = output[IMAG] = (double *)malloc(sizeof(double)*N);

  WN__ = -2*M_PI/N;
  WN__ = inv?-WN__:WN__;  /* IDFT */

  for(k=0;k<N;k++) {
    *oR = 0;
    *oI = 0;
    WN_ = WN__ * k;
    iR = input[REAL];
    iI = input[IMAG];
    for(n=0;n<N;n++) {
      WN = WN_ * n;
      c = cos(WN);
      s = sin(WN);
      *oR += *iR*c + *iI*s;
      *oI += -*iR*s + *iI*c;
      iR++;
      iI++;
    }
    if(inv) {  /* IDFT */
      *oR /= N;
      *oI /= N;
    }
    oR++;
    oI++;
  }

  return output;
}

