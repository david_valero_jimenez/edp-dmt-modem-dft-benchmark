#include "cli.h"


void help(char *bin_name) {
  fprintf(stderr, "Benchmark program for high-level algorithms for DFT computing.\n\n");
  fprintf(stderr, "Usage:\n  %s <N1> <N2> ...\n\n", bin_name);
  fprintf(stderr, "Output: Tabbed results written to screen and \"%s\" file.\n\n", DATA_NAME);
  fprintf(stderr, CREDITS);
}


int checkCommandLine(int argc, char **argv, unsigned int *NS, unsigned int **size) {
  int error = 0;
  int i;

  if(argc<2) {

    error = 1;

  } else {

    *NS = argc-1;

    *size = (unsigned int *)malloc(sizeof(unsigned int)**NS);

    for(i=0;i<*NS;i++) {
      argv++;
      (*size)[i] = atoi(*argv);
    }

  }

  return error;
}


void writeOutput(unsigned int *size, long **benchmark, unsigned int NS, unsigned int NT) {
  FILE *f_result;
  int i, j;

  f_result = fopen(DATA_NAME, "w+");

  printf("\n\n--- RESULTS ---\n\n\tDFT\tIDFT\tFFT-R2\tIFFT-R2\n");
  fprintf(f_result, "---\tDFT\tIDFT\tFFT\tIFFT\n");
  for(i=0;i<NS;i++) {
    printf("%i)", size[i]);
    fprintf(f_result, "%i", size[i]);
    for(j=0;j<NT;j++) {
      printf("\t%li", benchmark[i][j]);
      fprintf(f_result, "\t%li", benchmark[i][j]);
    }
    printf("\n");
    fprintf(f_result, "\n");
  }

  fclose(f_result);

  printf("\n Tabbed results written to \"%s\".\n\n", DATA_NAME);
}

