#include "fft.h"

double **fft_r2(int N, double **x) {
  return _fft_ifft_r2(N, x, 0);
}

double **ifft_r2(int N, double **X) {
  return _fft_ifft_r2(N, X, 1);
}


double **_fft_ifft_r2(int N, double **input, int inv) {
  double  WN__, WN_, **WN, *WNr, *WNi;
  double **output, *o1R, *o1I, *o2R, *o2I, *i1R, *i1I, *i2R, *i2I;
  double mult[2], **swap;
  int i, b, k;
  int S, B, Nb, MED = N>>1, MEDb;


  /* PHASE FACTOR COMPUTING */
  WN__ = -2*M_PI/N;
  WN__ = inv?-WN__:WN__;  /* IFFT */
  WN = (double **)malloc(sizeof(double *)*2);  /* k={0,...,N/2-1} */
  WN[REAL] = (double *)malloc(sizeof(double)*MED);
  WN[IMAG] = (double *)malloc(sizeof(double)*MED);
  for(k=0;k<MED;k++) {
    WN_ = WN__ * k;
    WN[REAL][k] = cos(WN_);
    WN[IMAG][k] = sin(WN_);
  }

  /* Input bitreverse ordering */
  bitReverse(N, input);

  /* Input and output matrices will swap in each stage */
  output = (double **)malloc(sizeof(double *)*2);
  output[REAL] = (double *)malloc(sizeof(double)*N);
  output[IMAG] = (double *)malloc(sizeof(double)*N);


  /*
    S: number of stages
    B: number of butterflies in each stage
    Nb: size of butterfly in each stage
  */
  S = log(N)/M_LN2;
  B = MED;
  Nb = N/B;
  for(i=0;i<S;i++) {  /* for each stage */
    MEDb = Nb>>1;

    if(i%2 == 0) {  /* input and output assignment */
      i1R = i2R = input[REAL]; i1I = i2I = input[IMAG];
      o1R = o2R = output[REAL]; o1I = o2I = output[IMAG];
    } else {
      i1R = i2R = output[REAL]; i1I = i2I = output[IMAG];
      o1R = o2R = input[REAL]; o1I = o2I = input[IMAG];
    }

    i2R += MEDb; i2I += MEDb;
    o2R += MEDb; o2I += MEDb;


    for(b=0;b<B;) {  /* for each butterfly */
      WNr = WN[REAL];
      WNi = WN[IMAG];

      for(k=0;k<MEDb;k++) {
	*o1R = *o2R = *i1R;
	*o1I = *o2I = *i1I;

	//if(B==MED) {  /** === k EQUALS 0 --> to include all k==0 cases and not only the first stage **/
	if(k==0) {
	  *o1R += *i2R; *o1I += *i2I;
	  *o2R -= *i2R; *o2I -= *i2I;
	} else {
	  mult[REAL] = *WNr * *i2R - *WNi * *i2I;
	  mult[IMAG] = *WNr * *i2I + *WNi * *i2R;
	  *o1R += mult[REAL]; *o1I += mult[IMAG];
	  *o2R -= mult[REAL]; *o2I -= mult[IMAG];
	}

	i1R++; i1I++; i2R++; i2I++;
	o1R++; o1I++; o2R++; o2I++;

	WNr+=B; WNi+=B;
      }

      b++;
      i1R+=MEDb; i1I+=MEDb; i2R+=MEDb; i2I+=MEDb;
      o1R+=MEDb; o1I+=MEDb; o2R+=MEDb; o2I+=MEDb;

    }


    B = B>>1;
    Nb = Nb<<1;

  }

  if(S%2 == 0) {  /* swap input_br and output if needed */
    swap = output;
    output = input;
    input = swap;
  }

  if(inv) {  /* IFFT */
    for(k=0;k<N;k++) {
      output[REAL][k] /= N;
      output[IMAG][k] /= N;
    }
  }


  free(WN[IMAG]);
  free(WN[REAL]);
  free(WN);


  return output;
}


void bitReverse(int N, double **input) {
  double **output, **swap, *i1R, *i1I, *i2R, *i2I, *oR, *oI;
  int i, b, k;
  int S, B, Nb, MEDb;

  output = (double **)malloc(sizeof(double *)*2);
  output[REAL] = (double *)malloc(sizeof(double)*N);
  output[IMAG] = (double *)malloc(sizeof(double)*N);

  S = log(N)/M_LN2;
  B = 1;
  Nb = N/B;
  for(i=1;i<S;i++) {  /* for each stage */
    MEDb = Nb>>1;

    i1R = i2R = input[REAL];
    i1I = i2I = input[IMAG];
    oR = output[REAL];
    oI = output[IMAG];

    for(b=0;b<B;) {  /* for each butterfly */

      *oR = *i1R;
      *oI = *i1I;
      oR++; oI++;
      i1R+=2; i1I+=2;
      i2R++; i2I++;

      for(k=0;k<MEDb-1;k++) {  /* copy of even elements */
	*oR = *i1R;
	*oI = *i1I;
	i1R+=2; i1I+=2;
	oR++; oI++;
      }

      for(k=0;k<MEDb-1;k++) {  /* copy of odd elements */
	*oR = *i2R;
	*oI = *i2I;	
	i2R+=2; i2I+=2;
	oR++; oI++;
      }

      *oR = *i2R;
      *oI = *i2I;
      i2R++; i2I++;
      oR++; oI++;
      
      b++;

    }

    swap = input;
    input = output;
    output = swap;

    B = B<<1;
    Nb = Nb>>1;

  }


  /* Copy output data to input and free allocated memory */
  if(S%2==0) {
    swap = input;
    input = output;
    output = swap;
  }

  for(i=0;i<N;i++) {
    input[REAL][i] = output[REAL][i];
    input[IMAG][i] = output[IMAG][i];
  }


  free(output[REAL]);
  free(output[IMAG]);
  free(output);

}

